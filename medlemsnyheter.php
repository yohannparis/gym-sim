<?php
	$page = 'medlemsnyheter';
	$description = '';
	require_once 'view/header.php';

	// Get the news which should be display on the homepage
	$news = request("
		SELECT inventory.*, images.path AS mainImage
			FROM `inventory`
			LEFT JOIN images ON images.newsid = inventory.id AND images.main = 1
		 WHERE inventory.show = 1
		 	 AND inventory.features = 0
		 ORDER BY inventory.added DESC;
	");
?>

<section class="news">

	<h1>Medlemsnyheter</h1>
	<ul>
		<?php foreach ($news as $anews) { ?>
			<li>
				<div class="news-image"><img src="<?='model/uploads/'.$anews['id'].'/'.$anews['mainImage'];?>" alt="<?=$anews['title'];?>"></div>
				<div class="news-info"><span>
					<h3><?=$anews['title'];?></h3>
					<p><?=$anews['subtitle'];?></p>
					<a href="newspage.php?news=<?=$anews['id'];?>">LÄS MER &#10142;</a>
				</span></div>
			</li>
		<?php } ?>
	</ul>

</section>

<?php require_once 'view/footer.php'; ?>
