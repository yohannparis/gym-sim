
			<footer>

				<div class="footer-address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
					<meta itemprop="addressCountry" content="Sweden">

					<h3>Kontakt</h3>
					<span itemprop="name">Gym &amp; Sim Vallentuna</span><br>
					<span itemprop="streetAddress"><?=$value['contact-address-street'];?></span><br>
					<span itemprop="postalCode"><?=$value['contact-address-postalCode'];?></span>
					<span itemprop="addressLocality"><?=$value['contact-address-city'];?></span><br>
					<a href="<?=$value['contact-address-link'];?>">Vägbeskrivning till oss</a>

					<a itemprop="telephone" href="tel:<?=$value['contact-contact-telephone'];?>">Tel: <?=$value['contact-contact-telephone'];?></a>
					<a itemprop="email" href="mailto:<?=$value['contact-contact-email'];?>">E-post: <?=$value['contact-contact-email'];?></a>
				</div>

				<div class="footer-hours">
					<h3>Ordinarie öppettider</h3>

					<table cellspacing="0" cellpadding="0">
						<thead>
							<tr>
								<td></td>
								<td>GYM</td>
								<td>SIM</td>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>MÅN-TOR</td>
								<td>06-22</td>
								<td>07-20</td>
							</tr>
							<tr>
								<td>FRE</td>
								<td>06-21</td>
								<td>07-20</td>
							</tr>
							<tr>
								<td>LÖR</td>
								<td>08-18</td>
								<td>08-17</td>
							</tr>
							<tr>
								<td>SÖN</td>
								<td>08-20</td>
								<td>08-17</td>
							</tr>
							<tr>
								<td colspan="3"><a target="_blank" href="http://view.pagetiger.com/GYMSIM-BOKEN/GYMSIM-BOKEN2015-04-18/page2.htm">STORHELGER &amp; SOMMAR</a></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="footer-externalLinks">
					<h3>Ytterligare länkar</h3>
					<a target="_blank" href="<?=$value['footer-externalLinks-01-link'];?>"><?=$value['footer-externalLinks-01-title'];?></a>
					<a target="_blank" href="<?=$value['footer-externalLinks-02-link'];?>"><?=$value['footer-externalLinks-02-title'];?></a>
					<a target="_blank" href="<?=$value['footer-externalLinks-03-link'];?>"><?=$value['footer-externalLinks-03-title'];?></a>
					<a target="_blank" href="/model/uploads/cafe/<?=$value['footer-cafe'];?>">Café</a>
				</div>

				<div class="footer-socialMedia">
					<?php if ($value['footer-socialMedia-youtube'] != ''){	?>
						<a class="footer-socialMedia-youtube" target="_blank" href="<?=$value['footer-socialMedia-youtube'];?>">Youtube</a>
					<?php } ?>
					<?php if ($value['footer-socialMedia-instagram'] != ''){	?>
						<a class="footer-socialMedia-instagram" target="_blank" href="<?=$value['footer-socialMedia-instagram'];?>">Instagram</a>
					<?php } ?>
					<?php if ($value['footer-socialMedia-facebook'] != ''){	?>
						<a class="footer-socialMedia-facebook" target="_blank" href="<?=$value['footer-socialMedia-facebook'];?>">Facebook</a>
					<?php } ?>
				</div>

			</footer>

		</div>
	</div>

	<script async src="//cdn.polyfill.io/v1/polyfill.js"></script>
	<!--[if lt IE 9]>
		<script async src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script async src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
	<![endif]-->

	<script>
		// ---- Menu code for Mobile only
		// ...Didn't wanted to had server detection for less than 200 bytes
		var mainNav = document.getElementsByClassName('navigation')[0];
		mainNav.addEventListener('click', function(event){
			// Only open and close if we click on the navigation button (:after) and not on the links
			if(event.srcElement.classList.contains('navigation')){
				document.getElementsByClassName('wrapper')[0].classList.toggle('nav-open');
			}
		});
	</script>

	<?php if($news['media'] === '') { ?>
		<script>
			// ---- Prepare the images
			var first = document.getElementsByClassName('module-slideshow-item')[0]; // Get the first element of the list
			first.className += ' active'; // Add a marker

			// ---- Carrousel
			function move(direction) {
				var list = document.getElementsByClassName('module-slideshow-item'); // Get the list of artworks
				var current = document.getElementsByClassName('active')[0]; // Get the current one
				current.className = 'module-slideshow-item'; // Remove is marker

				switch (direction){ // Select what direction we are going

					case 'next': // If we are moving forward
						var index = Array.prototype.slice.call(list).indexOf(current); // Position of the current one
						if (index >= list.length-1){ // But we are at the end of the list
							list[0].className += ' active'; // go back to the beginning
						} else {
							list[index+1].className += ' active'; // keep moving forward
						}
						break;

					case 'prev': // If we are going backward
						var index = Array.prototype.slice.call(list).indexOf(current); // Position of the current one
						if (index < 1){ // if we are at the beggining
							list[list.length-1].className += ' active'; // go to the end
						} else {
							list[index-1].className += ' active'; // keep moving backward
						}
						break;

					default: // Or it's a specific direction
						var index = Array.prototype.slice.call(list).indexOf(direction); // Find the position of the specific one
						list[index].className += ' active'; // display the selected one
						break;
				}
			}

			// On the press of one of the keyboard arrow
			document.onkeydown = function(e) {
				e = e || window.event;
				switch (e.keyCode) {
					case 37: // Left key
						move('prev');
						break;
					case 39: // Right key
						move('next');
						break;
				}
			};

			// On the click of one of the pastille
			var list = document.getElementsByClassName('module-slideshow-item'); // Get the list of artworks
			for (var i = list.length - 1; i >= 0; i--) { // Get trough it
				list[i].addEventListener('click', function(){ // Add a click on the event
					move(this); // And move to this specific pastille
				});
			};

			// On the click of one of the arrow
			document.getElementsByClassName('module-slideshow-arrow prev')[0].addEventListener('click', function() { move('prev');	});
			document.getElementsByClassName('module-slideshow-arrow next')[0].addEventListener('click', function() { move('next');	});

			// Automatically run trough the images
			//setInterval(function(){ move('next'); }, 5000);
		</script>
	<?php } ?>

</body>
</html>
<?php ob_end_flush(); // Send the Output Buffering ?>
