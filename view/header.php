<?php
	error_reporting(0); // Error Reporting
	require_once('admin/storage.php'); // Access to the database
	ob_start('ob_gzhandler'); // Compress output and turn on buffer
	header('content-type:text/html; content-language: sv; charset=utf-8'); // Set type and charset

	if(!isset($page) or $page != 'index'){$title = ucwords($page).' - '; } else { $title = ''; }
	if(!isset($description)){ $description = ''; }
	if(!isset($menu)){ $menu = ''; }

	// Get the latest storage value from the database
	$value = request("
		SELECT *
		  FROM storageCMS
		 WHERE storageCMS.key IN (
									'header-nav-01-title','header-nav-01-link',
									'header-nav-02-title','header-nav-02-link',
									'header-nav-03-title','header-nav-03-link',
									'header-nav-04-title','header-nav-04-link',
									'header-nav-05-title','header-nav-05-link',
									'header-nav-06-title','header-nav-06-link',
									'contact-address-street',	'contact-address-city',	'contact-address-postalCode', 'contact-address-link',
									'contact-contact-telephone', 'contact-contact-email',
									'footer-socialMedia-facebook', 'footer-socialMedia-instagram', 'footer-socialMedia-youtube',
									'footer-externalLinks-01-title','footer-externalLinks-01-link',
									'footer-externalLinks-02-title','footer-externalLinks-02-link',
									'footer-externalLinks-03-title','footer-externalLinks-03-link',
									'footer-cafe');
	", true);

?><!DOCTYPE html>
<html lang="sv" itemscope itemtype="http://schema.org/LocalBusiness">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" media="all" type="text/css" href="view/css/<?=$page;?>.css?v=2015-07-21">

	<title><?=$title;?>Gym &amp; Sim Vallentuna</title>
	<meta itemprop="url" property="og:url" content="http://gymochsim.se/">
	<meta itemprop="image" property="og:image" content="http://gymochsim.se/model/images/logo.png">
	<meta itemprop="name" property="og:site_name" content="Gym & Sim Vallentuna">
	<meta itemprop="description" name="description" property="og:title" content="<?=$description;?>">
	<meta property="og:description" content="<?=$description;?>">
	<meta property="og:locale" content="sv_SE">

	<link rel="shortcut icon" href="model/images/logo.png">
</head>
<body class="<?=$page;?>">

	<div class="wrapper">

		<div class="bounds">

			<header>
				<a class="header-logo" href="/"><img src="model/images/logo.png" alt="Gym &amp; Sim Vallentuna"></a>

				<ul class="navigation">
					<li class="navigation-logo"><img src="model/images/logo.png" alt="Gym &amp; Sim Vallentuna"></li>
					<li><a target="_blank" href="<?=$value['header-nav-01-link'];?>"><span><?=$value['header-nav-01-title'];?></span></a></li>
					<li><a target="_blank" href="<?=$value['header-nav-02-link'];?>"><span><?=$value['header-nav-02-title'];?></span></a></li>
					<li><a target="_blank" href="<?=$value['header-nav-03-link'];?>"><span><?=$value['header-nav-03-title'];?></span></a></li>
					<li><a target="_blank" href="<?=$value['header-nav-04-link'];?>"><span><?=$value['header-nav-04-title'];?></span></a></li>
					<li><a target="_blank" href="<?=$value['header-nav-05-link'];?>"><span><?=$value['header-nav-05-title'];?></span></a></li>
					<li><a target="_blank" href="<?=$value['header-nav-06-link'];?>"><span><?=$value['header-nav-06-title'];?></span></a></li>
				</ul>

			</header>
