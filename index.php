<?php
	$page = 'index';
	$description = '';
	require_once 'view/header.php';

	// Get the latest storage value from the database
	$valuePage = request("
		SELECT *
		  FROM storageCMS
		 WHERE storageCMS.key IN (
		 	'homepage-1-title', 'homepage-2-title', 'homepage-3-title', 'homepage-4-title');
	", true);

	// Get the news which should be display on the homepage
	$news = request("
		SELECT inventory.*, images.path AS mainImage
			FROM `inventory`
			LEFT JOIN images ON images.newsid = inventory.id AND images.main = 1
		 WHERE inventory.show = 1 /*AND inventory.homepage = 1*/
		 	 AND inventory.features = 0
		 ORDER BY inventory.added DESC
		 LIMIT 3;
	");

	// Get the features which should be display on the homepage
	$features = request("SELECT inventory.title FROM `inventory` WHERE inventory.features = 1;");
?>

<section class="index-features">

	<a href="motionsgym.php">
		<h2><?=$features[0]['title'];?></h2>
		<img src="model/images/homepage_motionsgym.jpg" alt="MOTIONGYM">
	</a>

	<a href="simhall.php">
		<h2><?=$features[1]['title'];?></h2>
		<img src="model/images/homepage_simhall.jpg" alt="SIMHALL">
	</a>

	<a href="challenge-zone.php">
		<h2><?=$features[2]['title'];?></h2>
		<img src="model/images/homepage_challengezone.jpg" alt="CHALLENGE ZONE">
	</a>

	<a href="ovrigt.php">
		<h2><?=$features[3]['title'];?></h2>
		<img src="model/images/homepage_ovright.jpg" alt="ÖVRIGT">
	</a>

</section>

<section class="news">

	<h2>Medlemsnyheter</h2>
	<ul>
		<?php foreach ($news as $anews) { ?>
			<li>
				<div class="news-image"><img src="<?='model/uploads/'.$anews['id'].'/'.$anews['mainImage'];?>" alt="<?=$anews['title'];?>"></div>
				<div class="news-info"><span>
					<h3><?=$anews['title'];?></h3>
					<p><?=$anews['subtitle'];?></p>
					<a href="newspage.php?news=<?=$anews['id'];?>">LÄS MER &#10142;</a>
				</span></div>
			</li>
		<?php } ?>
	</ul>

	<a class="button" href="/medlemsnyheter.php">Mer Nyheter</a>

</section>

<?php require_once 'view/footer.php'; ?>
