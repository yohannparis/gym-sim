<?php
	$page = 'feature';
	$description = '';
	require_once 'view/header.php';

	$id = 3;

	// Get the news which should be display on the homepage
	$news = request("
		SELECT inventory.*
			FROM `inventory`
		 WHERE inventory.id = $id;
	");

	// Get the pictures
	$pictures = request("SELECT * FROM images WHERE newsid = $id ORDER BY id DESC;");

	// Be sure that it is a container in case of one image
	if (isset($pictures['id'])) {
		$onePicture = $pictures;
		unset($pictures);
		$pictures[0] = $onePicture;
	}
?>

<h1><?=$news['title'];?> <span><?=$news['subtitle'];?></span></h1>

<?php if($news['media'] != '') { ?>

	<?php // Get the video ID out of the Youtube URL
		preg_match('%(?:http(?:s)?://)?(?:www\.)?youtu(?:be)?\.(?:com|be)/(?:watch\?v=)?([A-Za-z0-9-_]+)%', $news['media'], $video);
	?>

	<div class="module-youtube">
		<iframe src="https://www.youtube-nocookie.com/embed/<?=$video[1];?>?rel=0&amp;controls=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
	</div>

<?php } else { ?>

	<div class="module-slideshow module-slideshow-<?=count($pictures);?>">
		<a href="javascript:void(0)" class="module-slideshow-arrow prev">Previous</a>
		<ul>
			<?php foreach ($pictures as $picture){ ?>
				<li class="module-slideshow-item"><img src="<?='model/uploads/'.$picture['newsid'].'/'.$picture['path'];?>"></li>
			<?php } ?>
		</ul>
		<a href="javascript:void(0)" class="module-slideshow-arrow next">Next</a>
	</div>

<?php } ?>

<p><?=$news['content'];?></p>

<?php require_once 'view/footer.php'; ?>

