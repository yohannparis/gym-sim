<?php
/* Sessions */

	ini_set('session.use_only_cookies', true); // Starting the Session by using only cookies
	session_start();


/* LogOut System */

	if (isset($_GET['logout']) && $_SESSION['isLogin']){ // If the page receive the order to log out

		$_SESSION = array(); // Empty the Session
		if (ini_get("session.use_cookies")){ // Delete all information in the Cookies
			$params = session_get_cookie_params();
			setcookie(session_name(),'',time() - 42000,$params["path"],$params["domain"],$params["secure"],$params["httponly"]);
		}
		session_destroy(); // Destroy the Session
	}


/* LogIn System*/

	// The user want to log in, we get the username and the password
	if (isset($_POST['email']) && isset($_POST['password'])){

		// Get the latest storage value from the database
		$value = request("SELECT * FROM storageCMS WHERE storageCMS.key IN ('admin-login', 'admin-password');", true);

		if ($_POST['email'] === $value['admin-login'] && $_POST['password'] === $value['admin-password']){
			// Set the user is login
			$_SESSION['isLogin'] = true;
		} else {
			$_SESSION['isLogin'] = false;
		}
	}


/* Session Security */

	if (!isset($_SESSION['generated']) || $_SESSION['generated'] < (time()-30)){
		// The Session Id is regenerate every 30 sec to avoid Session theft attack
		session_regenerate_id();
		$_SESSION['generated'] = time();
	}
