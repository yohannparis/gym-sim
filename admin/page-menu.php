<?php
	// Build the SQL request to send
	$update = '';
	if (isset($_POST['header-nav-01-title'])){ $update .= "('header-nav-01-title','".mysql_escape_string($_POST['header-nav-01-title'])."'),"; }
	if (isset($_POST['header-nav-01-link'])){ $update .= "('header-nav-01-link','".mysql_escape_string($_POST['header-nav-01-link'])."'),"; }
	if (isset($_POST['header-nav-02-title'])){ $update .= "('header-nav-02-title','".mysql_escape_string($_POST['header-nav-02-title'])."'),"; }
	if (isset($_POST['header-nav-02-link'])){ $update .= "('header-nav-02-link','".mysql_escape_string($_POST['header-nav-02-link'])."'),"; }
	if (isset($_POST['header-nav-03-title'])){ $update .= "('header-nav-03-title','".mysql_escape_string($_POST['header-nav-03-title'])."'),"; }
	if (isset($_POST['header-nav-03-link'])){ $update .= "('header-nav-03-link','".mysql_escape_string($_POST['header-nav-03-link'])."'),"; }
	if (isset($_POST['header-nav-04-title'])){ $update .= "('header-nav-04-title','".mysql_escape_string($_POST['header-nav-04-title'])."'),"; }
	if (isset($_POST['header-nav-04-link'])){ $update .= "('header-nav-04-link','".mysql_escape_string($_POST['header-nav-04-link'])."'),"; }
	if (isset($_POST['header-nav-05-title'])){ $update .= "('header-nav-05-title','".mysql_escape_string($_POST['header-nav-05-title'])."'),"; }
	if (isset($_POST['header-nav-05-link'])){ $update .= "('header-nav-05-link','".mysql_escape_string($_POST['header-nav-05-link'])."'),"; }
	if (isset($_POST['header-nav-06-title'])){ $update .= "('header-nav-06-title','".mysql_escape_string($_POST['header-nav-06-title'])."'),"; }
	if (isset($_POST['header-nav-06-link'])){ $update .= "('header-nav-06-link','".mysql_escape_string($_POST['header-nav-06-link'])."'),"; }

	// If we need to update the database
	if ($update != ''){

		// Insert the updates in the request and remove the last coma
		$request = "
			INSERT INTO storageCMS (storageCMS.key,storageCMS.value) VALUES ".substr($update, 0, -1)."
			ON DUPLICATE KEY UPDATE storageCMS.key=VALUES(storageCMS.key),storageCMS.value=VALUES(storageCMS.value);
		";

		// Send the request
		$respond = request($request);
	}

	// Get the latest storage value from the database
	$value = request("
		SELECT *
		  FROM storageCMS
		 WHERE storageCMS.key IN (
			'header-nav-01-title', 'header-nav-01-link',
			'header-nav-02-title', 'header-nav-02-link',
			'header-nav-03-title', 'header-nav-03-link',
			'header-nav-04-title', 'header-nav-04-link',
			'header-nav-05-title', 'header-nav-05-link',
			'header-nav-06-title', 'header-nav-06-link');
	", true);
?>

<?php	if (isset($respond) && $respond){ ?>
	<div class="alert alert-success fade in">
		<strong>Well done!</strong> You successfully saved the contact page information.
		<a class="close" data-dismiss="alert" href="#">&times;</a>
	</div>
<?php } else if (isset($respond) && !$respond){ ?>
	<div class="alert alert-error fade in">
		<strong>Oh snap!</strong> Something went wrong, please try submitting again.
		<a class="close" data-dismiss="alert" href="#">&times;</a>
	</div>
<?php	} ?>

<form action="?p=menu" method="post" class="form-horizontal">

	<div class="tabbable">

		<div class="tab-content">

			<div class="alert alert-info" style="margin: 0 20px 20px;">
				<p>To use a link to a specific page of a PDF on Page Tiger, please use this format:</p>
				<p>http://view.pagetiger.com/GYMSIM-BOKEN/<strong>[PDF-NAME]</strong>/page<strong>[NUMBER]</strong>.htm</p>
				<p><br>
					Example: PDF <strong>GYMSIM-BOKEN2015-04-18</strong>, Page <strong>59</strong><br>
					<em>http://view.pagetiger.com/GYMSIM-BOKEN/<strong>GYMSIM-BOKEN2015-04-18</strong>/page<strong>59</strong>.htm</em>
				</p>
			</div>

		 	<div class="tab-pane active" id="menu">

		 		<?php for ($i=1; $i <= 6; $i++) { ?>

					<div class="control-group">
						<label class="control-label">Title <?=$i;?></label>
							<div class="controls"><input class="input-xlarge" type="text" name="header-nav-0<?=$i;?>-title" value="<?=$value["header-nav-0$i-title"];?>"></div>
					</div>
					<div class="control-group">
						<label class="control-label">Link <?=$i;?></label>
						<div class="controls"><input class="input-xlarge" type="text" name="header-nav-0<?=$i;?>-link" value="<?=$value["header-nav-0$i-link"];?>"></div>
					</div>
					<br><br>

				<?php } ?>

			</div>

		</div>

	</div>

	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Save changes</button>
	</div>
</form>
