<?php
/*  Copyright to Hughes Motor Products Inc.

	Developed by The Metrick System - http://metricksystem.com/
	Yohann Paris - developer@metricksystem.com
*/

	// Build the SQL request to send
	$update = '';
	if (isset($_POST['admin-login'])){ $update .= "('admin-login','".mysql_escape_string($_POST['admin-login'])."'),"; }
	if (isset($_POST['admin-password'])){ $update .= "('admin-password','".mysql_escape_string($_POST['admin-password'])."'),"; }

	// If we need to update the database
	if ($update != ''){

		// Insert the updates in the request and remove the last coma
		$request = "
			INSERT INTO storageCMS (storageCMS.key,storageCMS.value) VALUES ".substr($update, 0, -1)."
			ON DUPLICATE KEY UPDATE storageCMS.key=VALUES(storageCMS.key),storageCMS.value=VALUES(storageCMS.value);
		";

		// Send the request
		$respond = request($request);
	}

	// Get the latest storage value from the database
	$value = request("
		SELECT *
		  FROM storageCMS
		 WHERE storageCMS.key IN (	'admin-login',
									'admin-password');
	", true);
?>

<?php	if (isset($respond) && $respond){ ?>
			<div class="alert alert-success fade in">
				<strong>Well done!</strong> You successfully saved the Admin login information.
				<a class="close" data-dismiss="alert" href="#">&times;</a>
			</div>
<?php 	} else if (isset($respond) && !$respond){ ?>
			<div class="alert alert-error fade in">
				<strong>Oh snap!</strong> Something went wrong, please try submitting again.
				<a class="close" data-dismiss="alert" href="#">&times;</a>
			</div>
<?php	} ?>

	<form action="?p=admin" method="post" class="form-horizontal">

		<div class="tabbable">

			<div class="tab-content">

			 	<div class="tab-pane active" id="admin-login">

					<div class="control-group">
						<label class="control-label">Login</label>
						<div class="controls"><input class="input-xlarge" type="text" name="admin-login" value="<?=$value['admin-login'];?>" /></div>
					</div>
					<div class="control-group">
						<label class="control-label">Password</label>
						<div class="controls"><input class="input-xlarge" type="text" name="admin-password" value="<?=$value['admin-password'];?>" /></div>
					</div>

			 	</div>

			</div>

		</div>

		<div class="form-actions">
			<button type="submit" class="btn btn-primary">Save changes</button>
		</div>
	</form>


















