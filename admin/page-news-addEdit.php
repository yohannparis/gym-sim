<?php
/* --------------------------------------------------------------- Action -- */

	// Get if it's an edit or add
	$action = 'edit';
	if (isset($_GET['a']) and $_GET['a'] === 'add'){ $action = 'add'; }

/* --------------------------------------------------------------- Save all the information -- */

	// In the case of an edit of informtatio we will get the information and update the database
	if ($action === 'edit'){

		// Build the SQL request to send
		$edit = '';

		if (isset($_POST['news-title'])){ $edit .= "title='".mysql_escape_string($_POST['news-title'])."',"; }
		if (isset($_POST['news-subtitle'])){ $edit .= "subtitle='".mysql_escape_string($_POST['news-subtitle'])."',"; }
		if (isset($_POST['news-content'])){ $edit .= "content='".mysql_escape_string($_POST['news-content'])."',"; }
		if (isset($_POST['news-media'])){ $edit .= "media='".mysql_escape_string($_POST['news-media'])."',"; }

		// If we need to update the database
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			// Insert the updates in the request and remove the last coma
			$request = "UPDATE inventory SET ".substr($edit, 0, -1)." WHERE inventory.id = ".$_GET['id'].";";

			// Send the request
			$respond = request($request);
			$respondID = true;
		}

	// In case we add a new vehicle, we save all the information,
	// then we get back the id of the new vehicle to add pictures etc...
	} else if ($action === 'add'){

		// Build the SQL request to send
		$addTable = '';
		$addvalue = '';

		if (isset($_POST['news-title'])){ $addTable .= "inventory.title,"; $addvalue .= "'".mysql_escape_string($_POST['news-title'])."',"; }
		if (isset($_POST['news-subtitle'])){ $addTable .= "inventory.subtitle,"; $addvalue .= "'".mysql_escape_string($_POST['news-subtitle'])."',"; }
		if (isset($_POST['news-content'])){ $addTable .= "inventory.content,"; $addvalue .= "'".mysql_escape_string($_POST['news-content'])."',"; }
		if (isset($_POST['news-media'])){ $addTable .= "inventory.media,"; $addvalue .= "'".mysql_escape_string($_POST['news-media'])."',"; }

		// If we need to update the database
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			// We get the current time, we save it in a variables to use it to get the ID of the new vehicules back.
			$currentDate = date("Y-m-d H:i:s");

			// Insert the updates in the request and remove the last coma
			$request = "INSERT INTO inventory (".$addTable." inventory.show, inventory.added) VALUES (".$addvalue." '0', '".$currentDate."');";
			$respond = request($request);

			// Get the id of the vehicule
			$requestID = "SELECT inventory.id FROM inventory WHERE inventory.added LIKE '".$currentDate."';";
			$respondID = request($requestID);
		}
	}

/* --------------------------------------------------------------- Get all the information -- */

	// Get the news
	if ((isset($_GET['id']) and $_GET['id'] != '') or (isset($respondID['id']) and $respondID['id'] != '')){

		if (isset($_GET['id']) and $_GET['id'] != ''){ $id = $_GET['id']; }
		else if (isset($respondID['id'])){ $id = $respondID['id']; }

		$news = request("
			SELECT inventory.*, images.path AS mainImage
				FROM `inventory`
				LEFT JOIN images ON images.newsid = inventory.id AND images.main = 1
			 WHERE inventory.id LIKE '$id'
		");

		$pictures = request("SELECT * FROM images WHERE newsid = $id ORDER BY main DESC, id DESC;");
	}

/* --------------------------------------------------------------- User message -- */
?>

<?php
	if ((isset($respond) and $respond) and (isset($respondID) and $respondID)){
		// When the add have been done, we change the action to be 'edit'
		// The user can add picture, etc. Everything is more secure than adding more cars.
		$action = 'edit';
?>
	<div class="alert alert-success fade in">
		<strong>Well done!</strong> You successfully saved the information.
		<a class="close" data-dismiss="alert" href="#">&times;</a>
	</div>
<?php } else if ((isset($respond) and !$respond) or (isset($respondID) and !$respondID)){ ?>
	<div class="alert alert-error fade in">
		<strong>Oh snap!</strong> Something went wrong, please try submitting again.
		<a class="close" data-dismiss="alert" href="#">&times;</a>
	</div>
<?php	} ?>


<div class="tabbable">

	<ul class="nav nav-tabs">
		<?php if($action === 'add' or $action === 'edit'){ ?>
			<li class="active"><a href="#news-info" data-toggle="tab">Information</a></li>
		<?php } if($action === 'edit'){ ?>
			<li><a href="#news-pictures" data-toggle="tab">Pictures</a></li>
		<?php } ?>
	</ul>

	<div class="tab-content">

		<!-- Content -->
		<?php if($action === 'add' or $action === 'edit'){ ?>

			<div class="tab-pane active" id="news-info">

				<form action="?p=news-addEdit&a=<?=$action;?>&id=<?=$news['id'];?>" method="post" class="form-horizontal">

					<div class="control-group">
						<label class="control-label">Title</label>
						<div class="controls"><input class="input-xlarge" type="text" name="news-title" value="<?=$news['title'];?>" /></div>
					</div>

					<div class="control-group">
						<label class="control-label">Subtitle</label>
						<div class="controls"><input class="input-xlarge" type="text" name="news-subtitle" value="<?=$news['subtitle'];?>" /></div>
					</div>

					<div class="control-group">
						<label class="control-label">Text</label>
						<div class="controls">
							<textarea class="input span8" name="news-content" rows="5"><?=$news['content'];?></textarea>
							<p class="help-block">To divide into paragraphs, please add &lt;br&gt;&lt;br&gt; between sentences.</p>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Youtube link</label>
						<div class="controls">
							<input class="input-xlarge" type="text" name="news-media" value="<?=$news['media'];?>">
							<p class="help-block">The images will be shown if no links are provided</p>
						</div>
					</div>

					<div class="form-actions">
						<button type="submit" value="save" class="btn btn-primary" title="Save all the information.">Save</button>
						<a href="index.php?p=news" class="btn">Cancel</a>
					</div>

				</form>
			</div>
		<?php } ?>

		<!-- Images tab -->
		<?php if($action === 'edit'){ ?>

			<div class="tab-pane" id="news-pictures">

				<div class="alert alert-info" style="margin: 0 20px 20px;">
					When pictures are uploaded, please refresh the page to see them below.<br>
					Use the button 'main' to select the picture to be displayed on the listing.
				</div>

				<div id="file-uploader">
					<noscript>
						<p>Please enable JavaScript to use file uploader.</p>
						<!-- or put a simple form for upload here -->
					</noscript>
				</div>

				<script>
					function createUploader(){
						var uploader = new qq.FileUploader({
							element: document.getElementById('file-uploader'),
							action: 'uploader.php?newsID=<?=$news['id'];?>',
							debug: true
						});
					}

					// in your app create uploader as soon as the DOM is ready
					// don't wait for the window to load
					window.onload = createUploader;
				</script>

				<?php if (is_array($pictures)){ ?>

					<ul class="thumbnails">

						<?php
							if (isset($pictures['id'])) {
								$onePicture = $pictures;
								unset($pictures);
								$pictures[0] = $onePicture;
							}

							foreach ($pictures as $picture){
								// Display if the pictures is Main
								$main = '';
								if ($picture['main'] != 0){$main='active btn-warning';}
						?>
								<li class="span3" id="<?=$picture['id'];?>">
									<div class="thumbnail">
										<img src="../model/uploads/<?=$picture['newsid'];?>/<?=$picture['path'];?>" />
										<div class="caption">
											<div class="btn-toolbar">
												<span data-toggle="buttons-checkbox">
													<a class="btn btn-small <?=$main;?>" href="main" class-toggle="btn-warning" title="Display the picture on the listing and as first on the gallery page. Click to DISPLAY, unclick to HIDE.">Main</a>
												</span>
												<a class="btn btn-small btn-danger" href="delete" title="Delete the picture!">Delete</a>
											</div>
										</div>
									</div>
								</li>
						<?php }	?>
					</ul>
				<?php }	?>

			</div>
		<?php } ?>

	</div>
</div>

<script type="text/javascript">

	// Toggle Special/Featured in the database
	$('.btn-toolbar a').click(function(event) {

		// Stop the link!
		event.preventDefault();

		// Setup the AJAX call
		$.ajaxSetup({
			type: "POST",
			url: "model.php",
			dataType: 'json'
		});

		// Get the button and all the information to update
		var button = $(this);
		var action = button.attr('href');
		var id = button.parents('li').attr('id');
		var classToggle = button.attr('class-toggle');

		if (action === 'delete'){

			$.ajax({
				data: {action: 'delete', table: 'images', id: id}
				}).done(function(result){
					if (result.success){
					console.log('Database Request succeeded');
					$('li#'+id).remove();
				}
			});

		} else if (action === 'main'){

			// This function is identical of the one on the page-news.php file.
			// Please go there to understand the AJAX call

			if (button.hasClass('active')){
				$.ajax({
					data: {action: 'update', table: 'images', id: id, column: 'main', value: 0}
					}).done(function(result){
						if (result.success){
						button.removeClass(classToggle);
					} else {
						button.removeClass('active');
					}
				}).fail(function(){
					button.removeClass('active');
				});
			} else {
				$.ajax({
					data: {action: 'update', table: 'images', id: id, column: 'main', value: 1}
				}).done(function(result){
						if (result.success){
						button.addClass(classToggle);
					} else {
						button.addClass('active');
					}
				}).fail(function(){
					button.addClass('active');
				});
			}
		}
	});
</script>
