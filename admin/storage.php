<?php
// ---- Send Request
function request($request, $storage=false){

	// Create the Database connexion
	$link = mysql_connect('mysql2.tba.net','gymochsim.se','GRUa293p') or die('cannot connect');
	mysql_select_db('gymochsim' ,$link) or die('cannot select db');

	// Send the request
	$result = mysql_query($request);

	// If the result is a boolean
	if (is_bool($result)){
		$container = $result;
	} else {
		// Create the array container of the result
		$container = array();

		// If the result contains rows
		if (mysql_num_rows($result) > 1){
			while ($row = mysql_fetch_assoc($result)){

				// If we try to access the storage table we just need to return the key=>value pairs
				if ($storage){
					$container[$row['key']] = $row['value'];
				} else {
					// We add the rows one by one in the container
					array_push($container, $row);
				}
			}

		// Else we just add the only result on the first row of the container
		} else {
			$container = mysql_fetch_assoc($result);
		}
	}

	// Close the Database connexion
	mysql_close($link);

	// Return the container with the result easily accessible
	return $container;
}
