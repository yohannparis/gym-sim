<?php

// recursively remove a directory
function rrmdir($dir) {
	foreach(glob($dir . '/*') as $file) {
		if(is_dir($file))
			rrmdir($file);
		else
			unlink($file);
	}
	rmdir($dir);
}

// Include the DB access
require('storage.php');

// Respond with a JSON
header('Content-type: text/json; charset=utf-8');

// Set the result as false by default
$result = false;

// Verify if an action has been send
if (isset($_POST['action'])){

	// Which action?
	switch ($_POST['action']){

		// Update the database
		case 'update':

			// Verify if a column, a value and an ID has been send
			if (isset($_POST['table']) && isset($_POST['column']) && isset($_POST['value']) && isset($_POST['id'])){
				$query = "UPDATE ".$_POST['table']." SET ".$_POST['table'].".".$_POST['column']." = '".$_POST['value']."' WHERE ".$_POST['table'].".id LIKE ".$_POST['id'].";";
				$result = request($query);
			}
		break;

		// Delete from the database
		case 'delete':

			// Verify if a column, a value and an ID has been send
			if (isset($_POST['table']) && isset($_POST['id'])){
				$query = "DELETE FROM ".$_POST['table']." WHERE ".$_POST['table'].".id LIKE ".$_POST['id'].";";
				$result = request($query);

				// Delete products picture folder..
				if ($result && $_POST['table'] === 'inventory'){

					// Remove all the pictures in the database
					request("DELETE FROM images WHERE images.carid LIKE ".$_POST['id'].";");

					// Remove the folder...
					rrmdir('../model/uploads/'.$_POST['id']);
				}
			}
		break;
	}
}

if ($result){
	echo '{"success": true}';//, "query": "'.$query.'"}';
} else {
	echo '{"success": false}';//, "query": "'.$query.'"}';
}
