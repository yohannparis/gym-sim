<?php
// Build the SQL request to send
$update = '';

if (isset($_POST['contact-address-street'])){ $update .= "('contact-address-street','".mysql_escape_string($_POST['contact-address-street'])."'),"; }
if (isset($_POST['contact-address-city'])){ $update .= "('contact-address-city','".mysql_escape_string($_POST['contact-address-city'])."'),"; }
if (isset($_POST['contact-address-postalCode'])){ $update .= "('contact-address-postalCode','".mysql_escape_string($_POST['contact-address-postalCode'])."'),"; }
if (isset($_POST['contact-address-link'])){ $update .= "('contact-address-link','".mysql_escape_string($_POST['contact-address-link'])."'),"; }

/*
if (isset($_POST['contact-hours-week'])){ 			 $update .= "('contact-hours-week','".$_POST['contact-hours-week']."'),"; }
if (isset($_POST['contact-hours-saturday'])){ 		 $update .= "('contact-hours-saturday','".$_POST['contact-hours-saturday']."'),"; }
if (isset($_POST['contact-hours-sunday'])){ 		 $update .= "('contact-hours-sunday','".$_POST['contact-hours-sunday']."'),"; }
*/

if (isset($_POST['contact-contact-telephone'])){ $update .= "('contact-contact-telephone','".mysql_escape_string($_POST['contact-contact-telephone'])."'),"; }
if (isset($_POST['contact-contact-email'])){ $update .= "('contact-contact-email','".mysql_escape_string($_POST['contact-contact-email'])."'),"; }

if (isset($_POST['footer-socialMedia-facebook'])){ $update .= "('footer-socialMedia-facebook','".mysql_escape_string($_POST['footer-socialMedia-facebook'])."'),"; }
if (isset($_POST['footer-socialMedia-instagram'])){ $update .= "('footer-socialMedia-instagram','".mysql_escape_string($_POST['footer-socialMedia-instagram'])."'),"; }
if (isset($_POST['footer-socialMedia-youtube'])){ $update .= "('footer-socialMedia-youtube','".mysql_escape_string($_POST['footer-socialMedia-youtube'])."'),"; }

if (isset($_POST['footer-externalLinks-01-link'])){ $update .= "('footer-externalLinks-01-link','".mysql_escape_string($_POST['footer-externalLinks-01-link'])."'),"; }
if (isset($_POST['footer-externalLinks-01-title'])){ $update .= "('footer-externalLinks-01-title','".mysql_escape_string($_POST['footer-externalLinks-01-title'])."'),"; }
if (isset($_POST['footer-externalLinks-02-link'])){ $update .= "('footer-externalLinks-02-link','".mysql_escape_string($_POST['footer-externalLinks-02-link'])."'),"; }
if (isset($_POST['footer-externalLinks-02-title'])){ $update .= "('footer-externalLinks-02-title','".mysql_escape_string($_POST['footer-externalLinks-02-title'])."'),"; }
if (isset($_POST['footer-externalLinks-03-link'])){ $update .= "('footer-externalLinks-03-link','".mysql_escape_string($_POST['footer-externalLinks-03-link'])."'),"; }
if (isset($_POST['footer-externalLinks-03-title'])){ $update .= "('footer-externalLinks-03-title','".mysql_escape_string($_POST['footer-externalLinks-03-title'])."'),"; }

// If we need to update the database
if ($update != ''){

	// Insert the updates in the request and remove the last coma
	$request = "
		INSERT INTO storageCMS (storageCMS.key,storageCMS.value) VALUES ".substr($update, 0, -1)."
		ON DUPLICATE KEY UPDATE storageCMS.key=VALUES(storageCMS.key),storageCMS.value=VALUES(storageCMS.value);
	";

	// Send the request
	$respond = request($request);
}

// Get the latest storage value from the database
$value = request("
	SELECT *
	  FROM storageCMS
	 WHERE storageCMS.key IN (
	 							'contact-address-street',	'contact-address-city',	'contact-address-postalCode', 'contact-address-link',
								'contact-hours-week',	'contact-hours-saturday',	'contact-hours-sunday',
								'contact-contact-telephone', 'contact-contact-email',
								'footer-socialMedia-facebook', 'footer-socialMedia-instagram', 'footer-socialMedia-youtube',
								'footer-externalLinks-01-title','footer-externalLinks-01-link',
								'footer-externalLinks-02-title','footer-externalLinks-02-link',
								'footer-externalLinks-03-title','footer-externalLinks-03-link',
								'footer-cafe');
", true);
?>

<?php	if (isset($respond) && $respond){ ?>
	<div class="alert alert-success fade in">
		<strong>Well done!</strong> You successfully saved the contact page information.
		<a class="close" data-dismiss="alert" href="#">&times;</a>
	</div>
<?php } else if (isset($respond) && !$respond){ ?>
	<div class="alert alert-error fade in">
		<strong>Oh snap!</strong> Something went wrong, please try submitting again.
		<a class="close" data-dismiss="alert" href="#">&times;</a>
	</div>
<?php	} ?>

<form action="?p=contact" method="post" class="form-horizontal">

	<div class="tabbable">

		<ul class="nav nav-tabs">
			<li class="active"><a href="#contact-contact" data-toggle="tab">Contact</a></li>
			<!-- <li><a href="#contact-hours" data-toggle="tab">Hours</a></li> -->
			<li><a href="#contact-social" data-toggle="tab">Social</a></li>
			<li><a href="#contact-links" data-toggle="tab">Links</a></li>
			<li><a href="#contact-cafe" data-toggle="tab">Café</a></li>
		</ul>

		<div class="tab-content">

		 	<div class="tab-pane active" id="contact-contact">

				<div class="control-group">
					<label class="control-label">Telephone</label>
					<div class="controls"><input class="input-xlarge" type="tel" name="contact-contact-telephone" value="<?=$value['contact-contact-telephone'];?>" /></div>
				</div>
				<div class="control-group">
					<label class="control-label">Email</label>
					<div class="controls"><input class="input-xlarge" type="email" name="contact-contact-email"value="<?=$value['contact-contact-email'];?>" /></div>
				</div>
				<div class="control-group">
					<label class="control-label">Street</label>
					<div class="controls"><input class="input-xlarge" type="text" name="contact-address-street" value="<?=$value['contact-address-street'];?>" /></div>
				</div>
				<div class="control-group">
					<label class="control-label">City</label>
					<div class="controls"><input class="input-xlarge" type="text" name="contact-address-city" value="<?=$value['contact-address-city'];?>" /></div>
				</div>
				<div class="control-group">
					<label class="control-label">Postal Code</label>
					<div class="controls"><input class="input-xlarge" type="text" name="contact-address-postalCode" value="<?=$value['contact-address-postalCode'];?>" /></div>
				</div>
				<div class="control-group">
					<label class="control-label">Link</label>
					<div class="controls"><input class="input-xlarge" type="text" name="contact-address-link" value="<?=$value['contact-address-link'];?>" /></div>
				</div>

		 	</div>

		 	<?php /* <div class="tab-pane" id="contact-hours">

				<div class="control-group">
					<label class="control-label">Monday - Thursday</label>
					<div class="controls"><input class="input-xlarge" type="text" name="contact-hours-week" value="<?=$value['contact-hours-week'];?>" /></div>
				</div>
				<div class="control-group">
					<label class="control-label">Friday</label>
					<div class="controls"><input class="input-xlarge" type="text" name="contact-hours-friday" value="<?=$value['contact-hours-friday'];?>" /></div>
				</div>
				<div class="control-group">
					<label class="control-label">Saturday</label>
					<div class="controls"><input class="input-xlarge" type="text" name="contact-hours-saturday" value="<?=$value['contact-hours-saturday'];?>" /></div>
				</div>
				<div class="control-group">
					<label class="control-label">Sunday</label>
					<div class="controls"><input class="input-xlarge" type="text" name="contact-hours-sunday" value="<?=$value['contact-hours-sunday'];?>" /></div>
				</div>

		 	</div> */ ?>

		 	<div class="tab-pane" id="contact-social">

				<div class="control-group">
					<label class="control-label">Facebook</label>
					<div class="controls"><input class="input-xlarge" type="text" name="footer-socialMedia-facebook" value="<?=$value['footer-socialMedia-facebook'];?>" /></div>
				</div>
				<div class="control-group">
					<label class="control-label">Instagram</label>
					<div class="controls"><input class="input-xlarge" type="text" name="footer-socialMedia-instagram" value="<?=$value['footer-socialMedia-instagram'];?>" /></div>
				</div>
				<div class="control-group">
					<label class="control-label">YouTube</label>
					<div class="controls"><input class="input-xlarge" type="text" name="footer-socialMedia-youtube" value="<?=$value['footer-socialMedia-youtube'];?>" /></div>
				</div>

			</div>

		 	<div class="tab-pane" id="contact-links">

				<div class="control-group">
					<label class="control-label">Link 1</label>
					<div class="controls"><input class="input-xlarge" type="text" name="footer-externalLinks-01-title" value="<?=$value['footer-externalLinks-01-title'];?>" /></div>
				</div>
				<div class="control-group">
					<label class="control-label">URL</label>
					<div class="controls"><input class="input-xlarge" type="text" name="footer-externalLinks-01-link" value="<?=$value['footer-externalLinks-01-link'];?>" /></div>
				</div>

				<div class="control-group">
					<label class="control-label">Link 2</label>
					<div class="controls"><input class="input-xlarge" type="text" name="footer-externalLinks-02-title" value="<?=$value['footer-externalLinks-02-title'];?>" /></div>
				</div>
				<div class="control-group">
					<label class="control-label">URL</label>
					<div class="controls"><input class="input-xlarge" type="text" name="footer-externalLinks-02-link" value="<?=$value['footer-externalLinks-02-link'];?>" /></div>
				</div>

				<div class="control-group">
					<label class="control-label">Link 3</label>
					<div class="controls"><input class="input-xlarge" type="text" name="footer-externalLinks-03-title" value="<?=$value['footer-externalLinks-03-title'];?>" /></div>
				</div>
				<div class="control-group">
					<label class="control-label">URL</label>
					<div class="controls"><input class="input-xlarge" type="text" name="footer-externalLinks-03-link" value="<?=$value['footer-externalLinks-03-link'];?>" /></div>
				</div>

		 	</div>

		 	<div class="tab-pane" id="contact-cafe">

		 		<div style="margin: 0 20px 20px;">
					Upload a PDF file using the button:
				</div>

		 		<div id="file-uploader">
					<noscript>
						<p>Please enable JavaScript to use file uploader.</p>
						<!-- or put a simple form for upload here -->
					</noscript>
				</div>
				<br>

				<script>
					function createUploader(){
						var uploader = new qq.FileUploader({
							element: document.getElementById('file-uploader'),
							action: 'uploader.php?cafe=true',
							debug: true
						});
					}

					// in your app create uploader as soon as the DOM is ready
					// don't wait for the window to load
					window.onload = createUploader;
				</script>



		 	</div>

		</div>

	</div>

	<div class="form-actions">
		<button type="submit" class="btn btn-primary">Save changes</button>
	</div>
</form>
