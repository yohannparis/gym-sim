<?php
/* --------------------------------------------------------------- Action -- */

	// Get if it's an edit or add
	$edit = false;
	if (isset($_GET['a']) and $_GET['a'] === 'edit'){ $edit = true; }

/* --------------------------------------------------------------- Save all the information -- */

	// In the case of an edit of informtation we will update the database
	if ($edit){

		// Build the SQL request to send
		$edit = '';

		if (isset($_POST['news-title'])){ $edit .= "title='".mysql_escape_string($_POST['news-title'])."',"; }
		if (isset($_POST['news-subtitle'])){ $edit .= "subtitle='".mysql_escape_string($_POST['news-subtitle'])."',"; }
		if (isset($_POST['news-content'])){ $edit .= "content='".mysql_escape_string($_POST['news-content'])."',"; }
		if (isset($_POST['news-media'])){ $edit .= "media='".mysql_escape_string($_POST['news-media'])."',"; }

		// If we need to update the database
		if ($_SERVER['REQUEST_METHOD'] == 'POST') {

			// Insert the updates in the request and remove the last coma
			$request = "UPDATE inventory SET ".substr($edit, 0, -1)." WHERE inventory.id = ".$_GET['id']." AND inventory.features = 1;";

			// Send the request
			$respond = request($request);
			$respondID = true;
		}
	}

/* --------------------------------------------------------------- Get all the information -- */

	// In the case of an edit of informtation we will get the information
	if ((isset($_GET['id']) and $_GET['id'] != '') or (isset($respondID['id']) and $respondID['id'] != '')){

		if (isset($_GET['id']) and $_GET['id'] != ''){ $id = $_GET['id']; }
		else if (isset($respondID['id'])){ $id = $respondID['id']; }

		// To be sure we are in edit mode
		$edit = true;

		$news = request("SELECT inventory.* FROM `inventory` WHERE inventory.id LIKE '$id' AND inventory.features = 1;");
		$pictures = request("SELECT * FROM images WHERE newsid = $id ORDER BY main DESC, id DESC;");

	// Else we are dealing with the front page
	} else {
		// Get the allNews
		$allNews = request("SELECT inventory.* FROM `inventory` WHERE inventory.features = 1 ORDER BY inventory.id ASC;");

		// Home page images --- sorry for that crap, but I only have 2 hours to create that
		$homepageImg[1] = 'homepage_motionsgym.jpg';
		$homepageImg[2] = 'homepage_simhall.jpg';
		$homepageImg[3] = 'homepage_challengezone.jpg';
		$homepageImg[4] = 'homepage_ovright.jpg';
	}

/* --------------------------------------------------------------- Page -- */

	// In the case of an edit of informtation we will update the database
	if ($edit){
?>

	<?php	if ((isset($respond) and $respond) and (isset($respondID) and $respondID)){ ?>
		<div class="alert alert-success fade in">
			<strong>Well done!</strong> You successfully saved the information.
			<a class="close" data-dismiss="alert" href="#">&times;</a>
		</div>
	<?php } else if ((isset($respond) and !$respond) or (isset($respondID) and !$respondID)){ ?>
		<div class="alert alert-error fade in">
			<strong>Oh snap!</strong> Something went wrong, please try submitting again.
			<a class="close" data-dismiss="alert" href="#">&times;</a>
		</div>
	<?php	} ?>

	<div class="tabbable">

		<ul class="nav nav-tabs">
			<li class="active"><a href="#news-info" data-toggle="tab"><?=$news['title'];?> - Information</a></li>
			<li><a href="#news-pictures" data-toggle="tab"><?=$news['title'];?> - Pictures</a></li>
		</ul>

		<div class="tab-content">

			<!-- Content -->
			<div class="tab-pane active" id="news-info">

				<form action="?p=features&a=edit&id=<?=$news['id'];?>" method="post" class="form-horizontal">

					<div class="control-group">
						<label class="control-label">Title</label>
						<div class="controls"><input class="input-xlarge" type="text" name="news-title" value="<?=$news['title'];?>" /></div>
					</div>

					<div class="control-group">
						<label class="control-label">Subtitle</label>
						<div class="controls"><input class="input-xlarge" type="text" name="news-subtitle" value="<?=$news['subtitle'];?>" /></div>
					</div>

					<div class="control-group">
						<label class="control-label">Text</label>
						<div class="controls">
							<textarea class="input span8" name="news-content" rows="5"><?=$news['content'];?></textarea>
							<p class="help-block">To divide into paragraphs, please add &lt;br&gt;&lt;br&gt; between sentences.</p>
						</div>
					</div>

					<div class="control-group">
						<label class="control-label">Youtube link</label>
						<div class="controls">
							<input class="input-xlarge" type="text" name="news-media" value="<?=$news['media'];?>">
							<p class="help-block">The images will be shown if no links are provided</p>
						</div>
					</div>

					<div class="form-actions">
						<button type="submit" value="save" class="btn btn-primary" title="Save all the information.">Save</button>
						<a href="index.php?p=news" class="btn">Cancel</a>
					</div>

				</form>
			</div>

			<!-- Images tab -->
			<div class="tab-pane" id="news-pictures">

				<div class="alert alert-info" style="margin: 0 20px 20px;">
					When pictures are uploaded, please refresh the page to see them below.
				</div>

				<div id="file-uploader">
					<noscript>
						<p>Please enable JavaScript to use file uploader.</p>
						<!-- or put a simple form for upload here -->
					</noscript>
				</div>

				<script>
					function createUploader(){
						var uploader = new qq.FileUploader({
							element: document.getElementById('file-uploader'),
							action: 'uploader.php?newsID=<?=$news['id'];?>',
							debug: true
						});
					}

					// in your app create uploader as soon as the DOM is ready
					// don't wait for the window to load
					window.onload = createUploader;
				</script>

				<?php if (is_array($pictures)){ ?>

					<ul class="thumbnails">

						<?php
							if (isset($pictures['id'])) {
								$onePicture = $pictures;
								unset($pictures);
								$pictures[0] = $onePicture;
							}

							foreach ($pictures as $picture){
						?>
								<li class="span3" id="<?=$picture['id'];?>">
									<div class="thumbnail">
										<img src="../model/uploads/<?=$picture['newsid'];?>/<?=$picture['path'];?>" />
										<div class="caption">
											<div class="btn-toolbar">
												<a class="btn btn-small btn-danger" href="delete" title="Delete the picture!">Delete</a>
											</div>
										</div>
									</div>
								</li>
						<?php }	?>
					</ul>
				<?php }	?>
			</div>

		</div>
	</div>

	<script type="text/javascript">

		// Toggle Special/Featured in the database
		$('.btn-toolbar a').click(function(event) {

			// Stop the link!
			event.preventDefault();

			// Setup the AJAX call
			$.ajaxSetup({
				type: "POST",
				url: "model.php",
				dataType: 'json'
			});

			// Get the button and all the information to update
			var button = $(this);
			var action = button.attr('href');
			var id = button.parents('li').attr('id');
			var classToggle = button.attr('class-toggle');

			$.ajax({
				data: {action: 'delete', table: 'images', id: id}
				}).done(function(result){
					if (result.success){
					console.log('Database Request succeeded');
					$('li#'+id).remove();
				}
			});
		});
	</script>

<?php
	// Else we are dealing with the front page
	} else {
?>
	<ul class="thumbnails">
		<?php	foreach($allNews as $news){ ?>
			<li id="<?=$news['id'];?>" class="news span5">
				<div class="thumbnail">
					<img alt="<?=$news['title'];?>" src="../model/images/<?=$homepageImg[$news['id']];?>">
					<div class="caption">
						<h4><?=$news['title'];?></h4>
						<br/>
						<div class="btn-toolbar">
							<a class="btn" href="index.php?p=features&a=edit&id=<?=$news['id'];?>">Edit Information And Images</a>
						</div>
					</div>
				</div>
			</li>
		<?php } ?>
	</ul>
<?php } ?>
