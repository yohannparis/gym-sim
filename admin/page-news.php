<?php
// Get the allNews
$allNews = request("
	SELECT inventory.*, images.path AS mainImage
	  FROM `inventory`
	  LEFT JOIN images ON images.newsid = inventory.id AND images.main = 1
	 WHERE inventory.features = 0
	 ORDER BY inventory.added DESC;
");

if (isset($allNews['title'])){
	$alonenews = $allNews;
	unset($allNews);
	$allNews[0] = $alonenews;
}
?>

<ul class="thumbnails">

  <li class="span3">
  	<div id="addNews" class="thumbnail">
  		<a href="index.php?p=news-addEdit&a=add">
  			<h1>+</h1><br />
  			<h4>Add news</h4>
		</a>
  	</div>
  </li>

	<?php
		foreach($allNews as $news){

			$homepage = '';
			$show = '';
			if ($news['homepage'] != 0){ $homepage = 'active btn-warning'; }
			if ($news['show'] != 1){ $show = 'active btn-danger'; }
	?>
		<li id="<?=$news['id'];?>" class="news span3">
			<div class="thumbnail">
				<img alt="<?=$news['title'];?>" src="<?='../model/uploads/'.$news['id'].'/'.$news['mainImage'];?>">
				<div class="caption">
					<h4><?=$news['title'];?></h4>
					<h4><?=$news['subtitle'];?></h4>
					<br/>
					<div class="btn-toolbar">
						<span class="btn-group">
							<a class="btn btn-small" href="index.php?p=news-addEdit&a=edit&id=<?=$news['id'];?>" title="Edit information or add more pictures."><i class="icon-pencil"></i> Edit</a>
							<a class="btn btn-small" href="delete" title="Delete the news and all related pictures permanently. All information cannot be recovered."><i class="icon-trash"></i> Delete</a>
						</span>
						<span class="btn-group display-page" data-toggle="buttons-checkbox">
							<?php /*<a class="btn btn-small <?=$homepage;?>" href="homepage" class-toggle="btn-warning" title="Display the news on the homepage. Click to DISPLAY, unclick to HIDE."><i class="icon-star"></i></a>*/ ?>
							<a class="btn btn-small <?=$show;?>" href="show" class-toggle="btn-danger" title="HIDE the news from the website. Click to HIDE, unclick to DISPLAY."><i class="icon-ban-circle"></i> Hide</a>
						</span>
					</div>

				</div>
		    <div class="alert alert-block alert-error fade in hide">
					<h1 class="alert-heading">Warning!</h1>
					<p>
						You are going to permanently delete this news and all the pictures related to it.
					</p><p>
						<br/>
						<a href="close" class="btn btn-large btnClose">Cancel</a>
						<a href="delete" class="btn btn-large btn-danger btnDelete">Delete</a>
					</p>
				</div>
			</div>
		</li>
	<?php } ?>
</ul>

<script type="text/javascript">

	// In case of delete action, we open a alert box
	$('a[href="delete"]').click(function(event) {
		event.preventDefault();
		var id = $(this).parents('li').attr('id');
		$("#"+id+" .alert").show();
	});

	$('li.news .alert a.btnClose').click(function(event) {
		event.preventDefault();
		var id = $(this).parents('li').attr('id');
		$("#"+id+" .alert").hide();
	});

	// If he said yes, let's delete that vehicle from the database, and the directory associated with
	$('li.news .alert a.btnDelete').click(function(event) {
		event.preventDefault();
		var id = $(this).parents('li').attr('id');

		$.ajax({
			type: "POST",
			url: "model.php",
			dataType: 'json',
			data: {action: 'delete', table: 'inventory', id: id}
		}).done(function(result){
		    if (result.success){
		    	$('li#'+id).remove();
			}
		});
	});

	// Toggle Special/Featured in the database
	$('.display-page a').click(function(event) {

		// Stop the link!
		event.preventDefault();

		// Setup the AJAX call
		$.ajaxSetup({
			type: "POST",
			url: "model.php",
			dataType: 'json'
		});

		// Get the button and all the information to update
		var button = $(this);
		var action = 'update';
		var table = 'inventory';
		var column = button.attr('href');
		var id = button.parents('li').attr('id');
		var classToggle = button.attr('class-toggle');

		if (button.hasClass('active')){

			// In case we want to update the column 'show' we have to reverse the boolean value
			var value = 0;
			if (column === 'show'){value = 1;}

			// AJAX call
			$.ajax({
				data: {action: action, table: table, id: id, column: column, value: value}

			// In case of result
			}).done(function(result){

				// If the update was a success
				if (result.success){

					//console.log('Database Request succeeded');

					// Change the color of the button
					button.removeClass(classToggle);

				} else {

					//console.log('Database Request failed');

					// We keep the button as active to signal that the request failed
					button.removeClass('active');
				}

			// In case the AJAX call didn't succeeded
			}).fail(function(){

				//console.log('AJAX Request failed');

				// We keep the button as active to signal that the request failed
				button.removeClass('active');
			});

		} else {

			// In case we want to update the column 'show' we have to reverse the boolean value
			var value = 1;
			if (column === 'show'){value = 0;}

			// AJAX call
			$.ajax({
				data: {action: action, table: table, id: id, column: column, value: value}

			// In case of result
			}).done(function(result){

				// If the update was a success
				if (result.success){

					//console.log('Database Request succeeded');

					// Change the color of the button
					button.addClass(classToggle);

				} else {

					//console.log('Database Request failed');

					// We keep the button as active to signal that the request failed
					button.addClass('active');
				}

			// In case the AJAX call didn't succeeded
			}).fail(function(){

				//console.log('AJAX Request failed');

				// We keep the button as active to signal that the request failed
				button.addClass('active');
			});
		}
	});

</script>
































