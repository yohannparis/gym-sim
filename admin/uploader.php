<?php

// list of valid extensions, ex. array("jpeg", "xml", "bmp")
$allowedExtensions = array();
// max file size in bytes
$sizeLimit = 20 * 1024 * 1024;

require('upload.php');
require('storage.php');

$uploader = new qqFileUploader($allowedExtensions, $sizeLimit);

// check if it's for photos on the news or the cafe pdf
if(isset($_GET['newsID']) and $_GET['newsID'] != ''){

	// Call handleUpload() with the name of the folder, relative to PHP's getcwd()
	$result = $uploader->handleUpload('../model/uploads/'.$_GET['newsID'].'/');
	$query = request("INSERT INTO images (path, newsid) VALUES ('".$result['filename']."', ".$_GET['newsID'].");");

} else if(isset($_GET['cafe']) and $_GET['cafe']){

	$result = $uploader->handleUpload('../model/uploads/cafe/');
	$query = request("UPDATE storageCMS SET storageCMS.value='".$result['filename']."' WHERE storageCMS.key = 'footer-cafe';");
}

// to pass data through iframe you will need to encode all html tags
echo htmlspecialchars(json_encode($result['result']), ENT_NOQUOTES);
