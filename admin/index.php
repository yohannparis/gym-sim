<?php
require_once 'storage.php';
require_once 'session.php';
header('content-type:text/html; content-language: sv; charset=utf-8'); // Set type and charset

?><!DOCTYPE html>
<html lang="sv">
<head>
	<title>ADMIN - Gym &amp; Sim Vallentuna</title>
	<link rel="stylesheet" href="style/bootstrap.min.css">
	<link rel="stylesheet" href="style/main.css?v=2015-05-12">
	<link rel="stylesheet" href="style/fileuploader.css" >

	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
	<script src="bootstrap.min.js"></script>
	<script src="fileuploader.js"></script>
</head>
<body>
	<header>
		<img src="../model/images/logo.png" alt="Gym &amp; Sim Vallentuna">
	</header>

	<?php
		if(isset($_SESSION['isLogin']) && $_SESSION['isLogin']){

			// Get the page
			$page = 'news';
			if (isset($_GET['p']) && $_GET['p'] != ''){
				$page = $_GET['p'];
			}
	?>
		<nav class="btn-group">
			<a class="btn btn-large <?php if ($page == 'news') { echo 'disabled'; } ?>" href="?p=news">News</a>
			<a class="btn btn-large <?php if ($page == 'features') { echo 'disabled'; } ?>" href="?p=features">Features</a>
			<a class="btn btn-large <?php if ($page == 'menu') { echo 'disabled'; } ?>" href="?p=menu">Navigation</a>
			<a class="btn btn-large <?php if ($page == 'contact') { echo 'disabled'; } ?>" href="?p=contact">Contact</a>
			<a class="btn btn-large <?php if ($page == 'admin') { echo 'disabled'; } ?>" href="?p=admin">Password</a>
			<a class="btn btn-large btn-inverse" href="?logout">Logout</a>
		</nav>

		<div id="page" role="page-<?=$page;?>">
			<?php include_once('page-'.$page.'.php'); // Insert the concern page ?>
		</div>

	<?php	} else { ?>

		<div id="page" role="admin">
			<h1>Administration</h1>
			<form action="" method="post" class="form-inline">
				<input type="email" class="input" name="email" placeholder="Email" />
				<input type="password" class="input-small" name="password" placeholder="Password" />
				<input type="submit" class="btn btn-primary" value="Login" />
			</form>
		</div>

	<?php } ?>

	<script src="//cdn.polyfill.io/v1/polyfill.js"></script>
	<!--[if lt IE 9]>
		<script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/selectivizr/1.0.2/selectivizr-min.js"></script>
	<![endif]-->

	<script type="text/javascript">
		$(".alert").alert();
		window.setTimeout(function() {
			$(".alert-success").alert('close');
		}, 5000);
	</script>

</body>
</html>
